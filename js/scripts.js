// main js file


// update copyright year

const copyrightYear = document.querySelector('#copyrightYear');

function copyrightUpdate() {
	let dt = new Date().getFullYear();
	copyrightYear.innerHTML = dt;
}

copyrightUpdate();



// back to top

const topBtn = document.querySelector('#topBtn');

window.addEventListener("scroll", function(){
    if(window.scrollY > 400) {
        topBtn.style.opacity = "1";
        topBtn.style.right = "3%";
    } else {
        topBtn.style.opacity = "0";
        topBtn.style.right = "-83px";
    }
}, false);
